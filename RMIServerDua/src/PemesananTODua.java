public class PemesananTODua implements java.io.Serializable{
    private int id;
    private String waktukeberangkatan;
    private KeretaApiTO kereta;
    private PembeliTO pembeli;
    
    public PemesananTODua(){
        
    }
    
    public PemesananTODua(int id, KeretaApiTO kereta, PembeliTO pembeli){
        this.kereta = kereta;
        this.pembeli = pembeli;
        this.id = id;
    }
    
    public KeretaApiTO getKereta(){
        return kereta;
    }
    public PembeliTO getPembeli(){
        return pembeli;
    }
    public int getId(){
        return id;
    }
    public String getWaktuKeberangkatan(){
        return waktukeberangkatan;
    }
    
    public void setKereta(KeretaApiTO kereta){
        this.kereta = kereta;
    }
    public void setPembeli(PembeliTO pembeli){
        this.pembeli = pembeli;
    }
    public void setId(int id){
        this.id = id;
    }
    public void setWaktuKeberangkatan(String waktukeberangkatan){
        this.waktukeberangkatan = waktukeberangkatan;
    }
}
