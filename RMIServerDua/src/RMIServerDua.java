import java.rmi.registry.*;
import java.rmi.server.*;
import java.rmi.*;

public class RMIServerDua extends PembeliDua{
    public static void main(String[] args) throws RemoteException{
		Registry registry2 = LocateRegistry.createRegistry(1999);
		PembeliDua pembeli2 = new PembeliDua();
		KeretaApi kereta = new KeretaApi();
		PemesananDua pemesanan2 = new PemesananDua();
		PembeliInterface ipembeli2= (PembeliInterface) UnicastRemoteObject.exportObject(pembeli2,0);
		KeretaApiInterface ikereta= (KeretaApiInterface) UnicastRemoteObject.exportObject(kereta,0);
		PemesananInterfaceDua ipemesanan2= (PemesananInterfaceDua) UnicastRemoteObject.exportObject(pemesanan2,0);
		registry2.rebind("Pembeli2",ipembeli2);
		registry2.rebind("Kereta",ikereta);
		registry2.rebind("Pemesanan2",ipemesanan2);
		System.out.println("Server ready");
    }
}
