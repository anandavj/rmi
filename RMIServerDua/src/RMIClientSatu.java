import java.rmi.*;
import java.rmi.registry.*;

public class RMIClientSatu{
	public static void main(String[] args)throws Exception {  
      try {
         String host1 = args.length==0?"localhost":args[0];
         String host2 = args.length==0?"localhost":args[0];
         Registry registry1 = LocateRegistry.getRegistry(host1,Registry.REGISTRY_PORT);
         Registry registry2 = LocateRegistry.getRegistry(host2,1999);
         PembeliInterface stub4 = (PembeliInterface) registry2.lookup("Pembeli2");
         KeretaApiInterface stub5 = (KeretaApiInterface) registry2.lookup("Kereta");
         PemesananInterfaceDua stub6 = (PemesananInterfaceDua) registry2.lookup("Pemesanan2"); 
         PembeliTO pembeliSatu = new PembeliTO(1,"pembeli1");
         PembeliTO pembeliDua = new PembeliTO(2,"pembeli2");
         KeretaApiTO keretaSatu = new KeretaApiTO(1,"kereta1");
         KeretaApiTO keretaDua = new KeretaApiTO(2,"kereta2");
         PemesananTODua pemesananTiga = new PemesananTODua(4,keretaSatu,pembeliDua);
         PemesananTODua pemesananEmpat = new PemesananTODua(5,keretaDua,pembeliDua);
         stub4.add(pembeliSatu);
         stub4.add(pembeliDua);
         stub5.add(keretaSatu);
         stub5.add(keretaDua);
         stub6.add(pemesananTiga);
         stub6.add(pemesananEmpat);
         stub4.delete(1);
         stub5.delete(1);
         pembeliDua.setNama("pembeliUpdate");
         stub4.update(pembeliDua);
         stub5.update(keretaDua);
         pemesananEmpat.setPembeli(pembeliDua);
         pemesananEmpat.setKereta(keretaDua);
         stub6.update(pemesananEmpat);
      } catch (Exception e) { 
         System.err.println("Client exception: " + e.toString()); 
         e.printStackTrace(); 
      } 
   } 
}